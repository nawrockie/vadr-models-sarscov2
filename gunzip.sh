#!/bin/bash
for a in sarscov2; do
    gunzip $a.cm.gz
    gunzip $a.cm.i1f.gz
    gunzip $a.cm.i1i.gz
    gunzip $a.cm.i1m.gz
    gunzip $a.cm.i1p.gz
    gunzip $a.hmm.gz
    gunzip $a.hmm.h3f.gz
    gunzip $a.hmm.h3i.gz
    gunzip $a.hmm.h3m.gz
    gunzip $a.hmm.h3p.gz
done
